package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Cliente;

public interface ClienteDao extends CrudRepository<Cliente, Long> {
	public Cliente findByNombre_c(String nombre_c);//funcionalidad que no existe en extends se agregan

}
