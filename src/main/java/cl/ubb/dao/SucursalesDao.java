package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Sucursal;

public interface SucursalesDao extends CrudRepository<Sucursal, Long> {

	
}
