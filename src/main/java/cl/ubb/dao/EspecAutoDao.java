package cl.ubb.dao;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.EspecAuto;

public interface EspecAutoDao extends CrudRepository<EspecAuto, Long>{

	public ArrayList<EspecAuto> findByIdTypoAuto(long l);

}
