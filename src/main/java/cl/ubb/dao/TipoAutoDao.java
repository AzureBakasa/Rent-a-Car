package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.TipoAuto;

public interface TipoAutoDao extends CrudRepository<TipoAuto, Long>{
	
}
