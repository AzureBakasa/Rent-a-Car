package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.CategoriaCliente;

public interface CategoriaClienteDao extends CrudRepository<CategoriaCliente, Long> {
	public CategoriaCliente findByNombre_c(String nombre_c);//funcionalidad que no existe en extends se agregan

}