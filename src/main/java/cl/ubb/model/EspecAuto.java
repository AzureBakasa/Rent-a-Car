package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EspecAuto {
	@Id 
	@GeneratedValue 
	private long id;
	private long idTipoAuto;
	private String marca;
	private String modelo;
	private int ano;
	
	//Acceso
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public long getIdTipoAuto() {
		return idTipoAuto;
	}
	public void setIdTipoAuto(long idTipoAuto) {
		this.idTipoAuto = idTipoAuto;
	}
}
