package cl.ubb.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class CategoriaCliente {
	
	@Id
	@GeneratedValue
	private long id_c_c;
	private String nombre_c_c;
	
	public CategoriaCliente() {

	}
	//Acceso
	

	public long getId_c() {
		return id_c_c;
	}
	public void setId_c(long id_c_c) {
		this.id_c_c = id_c_c;
	}
	

	public String getNombre_c() {
		return nombre_c_c;
	}
	public void setNombre_c(String nombre_c_c) {
		this.nombre_c_c = nombre_c_c;
	}



	
}