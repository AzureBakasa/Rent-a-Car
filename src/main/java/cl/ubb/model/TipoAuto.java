package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TipoAuto {
	@Id 
	@GeneratedValue 
	private long id;
	private String nombre;
	private String tipotransmision;
	private String tipocombustible;
	private String bolsas;
	private int pasajeros;
	private int preciodiario;
	
	//Acceso
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoTransmision() {
		return tipotransmision;
	}
	public void setTipoTransmision(String tipotransmision) {
		this.tipotransmision = tipotransmision;
	}
	public String getTipoCombustible() {
		return tipocombustible;
	}
	public void setTipoCombustible(String tipocombustible) {
		this.tipocombustible = tipocombustible;
	}
	public String getBolsas() {
		return bolsas;
	}
	public void setBolsas(String bolsas) {
		this.bolsas = bolsas;
	}
	public int getPasajeros() {
		return pasajeros;
	}
	public void setPasajeros(int pasajeros) {
		this.pasajeros = pasajeros;
	}
	public int getPreciodiario() {
		return preciodiario;
	}
	public void setPreciodiario(int preciodiario) {
		this.preciodiario = preciodiario;
	}
	
}
