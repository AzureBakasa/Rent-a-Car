package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Cliente {
	
	@Id
	@GeneratedValue
	private long id_c;
	private String nombre_c;
	private long cellPhone;
	private String email;
	
	public Cliente() {

	}
	//Acceso
	
	//get y set del ID del CLIENTE
	public long getId() {
		return id_c;
	}
	public void setId(long id_c) {
		this.id_c = id_c;
	}
	
	//get y set del NOMBRE del CLIENTE
	public String getNombre_c() {
		return nombre_c;
	}
	public void setNombre_c(String nombre_c) {
		this.nombre_c = nombre_c;
	}
	//get y set de cellPhone
	public Long getcellPhone() {
		return cellPhone;
	}
	public void setcellPhone(int i) {
		this.cellPhone = i;
	}
	//get y set de EMAIL
	public String getemail() {
		return email;
	}
	public void setemail(String email) {
		this.email = email;
	}
}
	
	
	
	
	
	
	
	
