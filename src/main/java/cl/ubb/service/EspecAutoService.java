package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.EspecAutoDao;
import cl.ubb.model.EspecAuto;

public class EspecAutoService {
	private EspecAutoDao especAutoDao;
	@Autowired
	public ArrayList<String> retornarPorTipoAuto(Long l) {
		ArrayList<EspecAuto> receptor=(ArrayList<EspecAuto>)especAutoDao.findByIdTypoAuto(l);
		//Se sigue usando un array por que no se devuelve id
		ArrayList<String> regreso=new ArrayList<String>();
		for(int i=0;i<receptor.size();i++){
			regreso.add(receptor.get(i).getMarca());
			regreso.add(receptor.get(i).getModelo());
			regreso.add(Integer.toString(receptor.get(i).getAno()));
		}
		return regreso;
	}
	

}
