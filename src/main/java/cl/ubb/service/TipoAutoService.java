package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.TipoAutoDao;
import cl.ubb.model.TipoAuto;

public class TipoAutoService {
	private TipoAutoDao tipoAutoDao;
	@Autowired
	public ArrayList<String> retornarTiposAuto(){
	//similar a el servicio 3 mismo caso. No entendi el dato de entrada categoria, 
	//ya que las salidas eran de TypeCar y Typecar no tenia ninguna categoria, 
	//no existia ninguna categoria de tipos de auto.
		ArrayList<TipoAuto> receptor=(ArrayList<TipoAuto>)tipoAutoDao.findAll();
		ArrayList<String> regreso=new ArrayList<String>();
		for(int i=0;i<receptor.size();i++){
			regreso.add(Long.toString(receptor.get(i).getId()));
			regreso.add(receptor.get(i).getNombre());
			regreso.add(receptor.get(i).getTipoTransmision());
			regreso.add(receptor.get(i).getTipoCombustible());
			regreso.add(Integer.toString(receptor.get(i).getPasajeros()));
		}
		return regreso;
	}
}
