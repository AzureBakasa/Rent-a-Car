package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.CategoriaClienteDao;
import cl.ubb.model.CategoriaCliente;




public class CategoriaClienteService {
	
	private static  CategoriaClienteDao CategoriaClienteDao;
	@Autowired
	
	public CategoriaCliente crearCategoriaCliente(CategoriaCliente ccliente) {
		return CategoriaClienteDao.save(ccliente);
	}
	
	public CategoriaCliente obtenerCliente(Long id_c) {
		CategoriaCliente ccliente=new CategoriaCliente();
		ccliente=CategoriaClienteDao.findOne(id_c);
		return ccliente;
	}
	
	public ArrayList<CategoriaCliente> obtenerTodasLasCategorias() {
		return (ArrayList<CategoriaCliente>) CategoriaClienteDao.findAll();
	
	}
}