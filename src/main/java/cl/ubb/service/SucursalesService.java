package cl.ubb.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import cl.ubb.dao.SucursalesDao;
import cl.ubb.model.Sucursal;

public class SucursalesService {
	private SucursalesDao sucursalesDao;
	@Autowired
	public ArrayList<String> retornarIdYCiudad(){
		//me costo ver la logica de esta funcion, no se me ocurrio nada mas simple ni una mejor forma de hacerlo(para refactorizar)
		ArrayList<Sucursal> receptor = (ArrayList<Sucursal>) sucursalesDao.findAll();
		ArrayList<String> regreso=new ArrayList<String>();
		for(int i=0;i<receptor.size();i++){
			regreso.add(Long.toString(receptor.get(i).getId()));
			regreso.add(receptor.get(i).getCiudad());
		}
		return regreso;
	}
			
}
