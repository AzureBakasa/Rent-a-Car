package cl.ubb.rentacar;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ClienteDao;
import cl.ubb.dao.CategoriaClienteDao;
import cl.ubb.model.Cliente;
import cl.ubb.model.CategoriaCliente;
import cl.ubb.service.CategoriaClienteService;
import cl.ubb.service.ClienteService;


@RunWith(MockitoJUnitRunner.class)
public class ClienteTest {
	
	@Mock
	private ClienteDao clienteDao;
	private CategoriaCliente CategoriaClienteDao;
	
	@InjectMocks
	private ClienteService clienteService;
	private CategoriaClienteService CategoriaClienteService;
	
	@Test
	public void RegistrarCliente () {
		//arrange
		Cliente cliente = new Cliente();
		cliente.setId(123);
		cliente.setNombre_c("Rolando");
		cliente.setcellPhone(1234);
		cliente.setemail("rolo@rolo.cl");
		//act
		when(clienteDao.save(cliente)).thenReturn(cliente);
		Cliente clienteCreado=clienteService.crearCliente(cliente);
		//assert
		assertNotNull(clienteCreado);
		
		
		
	}
	@Test
	public void IndicarSiUnClienteSeEncuentraRegistrado(){
		//arrange
		Cliente cliente = new Cliente();
		cliente.setId(123);
		cliente.setNombre_c("Rolando");
		cliente.setcellPhone(1234);
		cliente.setemail("rolo@rolo.cl");
		//act
		when(clienteDao.findOne(anyLong())).thenReturn(cliente);
		Cliente ClienteRetornado=clienteService.obtenerCliente(cliente.getId());
		//assert
		assertNotNull(ClienteRetornado);
	}
	
	@Test
	public void ListarCategoriasDeClientes(){
		//arrange
		ArrayList<Cliente> misClientes = new ArrayList<Cliente>();
		//act
		when(clienteDao.findAll()).thenReturn(misClientes);
		ArrayList<CategoriaCliente> resultado=CategoriaClienteService.obtenerTodasLasCategorias();
		//assert
		assertNotNull(resultado);
		assertEquals(misClientes,resultado);
	}

	
	

}
