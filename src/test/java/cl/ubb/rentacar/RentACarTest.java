package cl.ubb.rentacar;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.EspecAutoDao;
import cl.ubb.dao.SucursalesDao;
import cl.ubb.dao.TipoAutoDao;
import cl.ubb.model.EspecAuto;
import cl.ubb.model.Sucursal;
import cl.ubb.model.TipoAuto;
import cl.ubb.service.EspecAutoService;
import cl.ubb.service.SucursalesService;
import cl.ubb.service.TipoAutoService;

@RunWith(MockitoJUnitRunner.class)
public class RentACarTest {
	
	@Mock private SucursalesDao sucursalesDao;
	@Mock private TipoAutoDao tipoAutoDao;
	@Mock private EspecAutoDao especAutoDao;
	
	@InjectMocks private SucursalesService sucursalesService;	
	@InjectMocks private TipoAutoService tipoAutoService;
	@InjectMocks private EspecAutoService especAutoService;
	
	@Test
	public void listarSucursalesDevuelveIdCiudad() {
		//arrange
		Sucursal sucursala=new Sucursal();
		Sucursal sucursalb=new Sucursal();
		sucursala.setId(1l);
		sucursala.setCiudad("Concepcion");
		sucursalb.setId(2l);
		sucursalb.setCiudad("Chillan");
		ArrayList<Sucursal> sucursales=new ArrayList<Sucursal>();//Resultado a mockear
		sucursales.add(sucursala);
		sucursales.add(sucursalb);
		ArrayList<String> misSucursalesPar = new ArrayList<String>();//Variable de control
		misSucursalesPar.add("1");
		misSucursalesPar.add("Concepcion");
		misSucursalesPar.add("2");
		misSucursalesPar.add("Chillan");
		ArrayList<String> resultado = new ArrayList<String>();
		//act
		when(sucursalesDao.findAll()).thenReturn(sucursales);
		resultado=sucursalesService.retornarIdYCiudad();//funcionalidad
		//assert
		assertNotNull(resultado);
		assertEquals(misSucursalesPar,resultado);
	}
	@Test
	public void listarTipoAuto() {
		//Devuelve id, nombre, tipo de transmision, tipo de combustible, numero pasajeros
		//arrange
		TipoAuto tipoa = new TipoAuto();
		TipoAuto tipob = new TipoAuto();
		tipoa.setId(1l);
		tipoa.setNombre("Tipo a");
		tipoa.setTipoTransmision("Automatica");
		tipoa.setTipoCombustible("Diesel");
		tipoa.setPasajeros(4);
		tipob.setId(2l);
		tipob.setNombre("Tipo b");
		tipob.setTipoTransmision("Mecanico");
		tipob.setTipoCombustible("Gasolina");
		tipob.setPasajeros(6);
		ArrayList<TipoAuto> tipos= new ArrayList<TipoAuto>();//resultado a mockear
		tipos.add(tipoa);
		tipos.add(tipob);
		ArrayList<String> misTipos = new ArrayList<String>();//Variable de control
		misTipos.addAll(Arrays.asList("1","Tipo a","Automatica","Diesel","4"));
		misTipos.addAll(Arrays.asList("2","Tipo b","Mecanico","Gasolina","6"));
		//act
		when(tipoAutoDao.findAll()).thenReturn(tipos);//mockeo
		ArrayList<String> resultado=tipoAutoService.retornarTiposAuto();//funcionalidad
		//assert
		assertNotNull(resultado);
		assertEquals(misTipos,resultado);
	}
	@Test
	public void listarAutoPorTipo(){
		//arrange
		//Datos de entrada: identificador de auto Datos de salida: marca, modelo, año
		EspecAuto especa=new EspecAuto();
		EspecAuto especb=new EspecAuto();
		especa.setId(1l);
		especa.setIdTipoAuto(1l);
		especa.setMarca("Nisan");
		especa.setModelo("A103");
		especa.setAno(2001);
		especb.setId(2l);
		especb.setIdTipoAuto(1l);
		especb.setMarca("BMW");
		especb.setModelo("X30");
		especb.setAno(2004);
		ArrayList<EspecAuto> especificacion=new ArrayList<EspecAuto>();//Resultado a mockear
		especificacion.add(especa);
		especificacion.add(especb);
		ArrayList<String> miEspec = new ArrayList<String>();//Variable de control 
		miEspec.addAll(Arrays.asList("Nisan","A103","2001"));
		miEspec.addAll(Arrays.asList("BMW","X30","2004"));
		//act
		when(especAutoDao.findByIdTypoAuto(anyLong())).thenReturn(especificacion);//mockeo
		ArrayList<String> resultado=especAutoService.retornarPorTipoAuto(1l);//funcionalidad
		//assert
		assertEquals(miEspec,resultado);		
	}

}
